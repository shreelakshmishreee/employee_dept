import sqlite3

conn = sqlite3.connect('employee.db')
cursor = conn.cursor()

# a) Create a Table Employee which will have the four columns, i.e., Name, ID, salary, and Department_id.
cursor.execute("""DROP TABLE IF EXISTS Employee""")
employee_sql = """
CREATE TABLE Employee (
Employee_Id text NOT NULL ,
Employee_Name text NOT NULL ,
Salary integer NOT NULL ,
Dept_Id integer NOT NULL
)"""
cursor.execute(employee_sql)

# b) Add a new column ‘City’ to the Table Employee
add_column = """ALTER TABLE Employee ADD COLUMN City text NOT NULL"""
cursor.execute(add_column)

# c) Insert 5 records into this table
employee_query = " INSERT INTO Employee ( Employee_Id ,Employee_Name ,Salary ,Dept_Id ,City ) VALUES (?,?,?,?,?)"
cursor.execute(employee_query,( 101 , 'Radhika', 40000 , 1 , 'Kazhakoottam' ))
cursor.execute(employee_query,( 102 , 'Rohan' , 35000 , 2 , 'Varkala' ))
cursor.execute(employee_query,( 103 , 'Aaradhya' ,42000 , 3 , 'Attingal' ))
cursor.execute(employee_query,( 104 , 'Aayush' , 45000 , 5, 'Sreekaryam' ))
cursor.execute(employee_query,( 105 , 'Keerthy' , 38000 , 4 , 'Kovalam' ))

# d) Read the Name, ID, and Salary from the Employee table and print it
cursor.execute( "select Employee_Name,Employee_Id,Salary FROM Employee")
employee_list = cursor.fetchall()
print('\n')
for row in employee_list:
    print(row)


# e) Print the details of employees whose names start with ‘j’ (or any letter input by the user)
letter = input("\nEnter the letter to print the details of employees whose names start with that letter : ")
cursor.execute( f"select Employee.Employee_Name,Employee.Employee_Id,Employee.Salary,Employee.Dept_Id FROM Employee "
                f"WHERE Employee.Employee_Name LIKE '{letter}%' ")
employee_list = cursor.fetchall()
for row in employee_list:
    print(row)

# f) Print the details of employees with ID’s inputted by the user
id_num = input("\nEnter the required ID number to display employee details : ")
cursor.execute( f"select Employee_Name,Employee_Id,Salary,Dept_Id FROM Employee WHERE Employee_Id = {id_num}")
employee_list = cursor.fetchall()
for row in employee_list:
    print(row)

# g) Change the name of the employee whose ID is input by the user
change_name_id = input("\nEnter the ID number to change the employee name : ")
change_name = input("Enter the required name : ")
cursor.execute(f"update Employee SET Employee_Name= '{change_name}' WHERE Employee_Id={change_name_id}")
cursor.execute( "select* FROM Employee")
employee_list = cursor.fetchall()
for row in employee_list:
    print(row)

# a) Create one more Table called Departments with two columns Department_id and Department_name
depts_sql = """
CREATE TABLE IF NOT EXISTS Departments (
Department_Id integer NOT NULL ,
Department_Name text NOT NULL 
)"""
cursor.execute(depts_sql)

# b) Insert 5 records into this table.
dept_query = " INSERT INTO Departments ( Department_Id ,Department_Name ) VALUES (?,?)"
cursor.execute(employee_query,( 1 , 'Accounts Department' ,'' ,'' ,'' ))
cursor.execute(employee_query,( 2 , ' Filing Department','' ,'' ,''   ))
cursor.execute(employee_query,( 3 , 'Public Relations Department' ,'' ,'' ,''  ))
cursor.execute(employee_query,( 4 , 'Marketing Department' ,'' ,'' ,'' ))
cursor.execute(employee_query,( 5 , 'Law Department','' ,'' ,''  ))

# c) To connect both the tables
cursor.execute("SELECT Dept_Id ,Departments.Department_Id FROM Employee"
               " INNER JOIN Departments ON Dept_Id = Departments.Department_Id")

# d) Print the details of all the employees in a particular department (Department_id is input by the user)
id_number = input("\nEnter the Department ID to display the details of the employee : ")
cursor.execute( f"select Employee_Name,Employee_Id,Salary,Dept_Id FROM  Employee WHERE Dept_Id = {id_number}")
employee_list = cursor.fetchall()
for row in employee_list:
    print(row)